Jactry's Emacs config on OSX
=======

## 使用插件
* auto-complete
* yasnippet
* color-theme
* tabbar
* weibo-emacs
* auto-highlight-symbol

## 修改内容
* 所用color-theme主题 molokai 修正中文显示不正常的问题
